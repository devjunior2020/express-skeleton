require('dotenv').config()
const path = require('path')

module.exports = {
  // پورت سرویس دهی سرور
  port: process.env.SERVER_PORT || 3000,

  // آدرس ارتباط با پایگاه داده به همراه نام دیتابیس
  mongo_uri: process.env.MONGO_URI,

  // کلید قدیمی تولید توکن و هش کردن پسوورد
  secret: process.env.SECRET_KEY,

  // نسخه برنامه
  versionEn: 'version 0.0.1 © December 2020',

  // تنظیمات اختیاری رابط کار با پایگاه داده مونگو
  mongo_opt: {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  },

  // تنظیمات اختیاری رابط های نرم افزاری
  // CORS === Cross-origin resource sharing
  cors_opt: {
    // Access-Control-Allow-Origin
    origin: '*',
    // Access-Control-Allow-Methods
    methods: 'GET,PUT,POST,DELETE',
    // Access-Control-Allow-Headers
    allowedHeaders: 'Content-Type,x-access-token,X-Requested-With',
    // Access-Control-Expose-Headers
    // exposedHeaders: "Content-Range,X-Content-Range",
    // Access-Control-Allow-Credentials
    // credentials: true,
    // Access-Control-Max-Age
    // maxAge: 10,
    // Pass the CORS preflight response to the next handler.
    preflightContinue: false,
    // Provides a status code to use for successful OPTIONS requests,
    // since some legacy browsers (IE11, various SmartTVs) choke on 204.
    optionsSuccessStatus: 204
  },

  // آدرس پوشه های مهم در بدنه پروژه
  path: {
    models: path.resolve('./modules/models'),
    controllers: path.resolve('./modules/controllers'),
    utils: path.resolve('./modules/utils'),
    middlewares: path.resolve('./modules/middlewares')
    // routes: path.resolve('./modules/routes'),
  },

  // تولید عدد تصادفی
  getRandNum: () => Math.floor(10000000 + Math.random() * 90000000).toString()
}
