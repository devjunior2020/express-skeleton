const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const compression = require('compression')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cors = require('cors')

global.cfg = require('./cfg')

global.masterModel = require(`${cfg.path.models}/_masterModel`)

const routes = require('./modules/routes/_masterRouter')

const app = express()

mongoose.connect(cfg.mongo_uri, cfg.mongo_opt, err => {
  if (err) console.log('mongoDB.error : ', err)
  else console.log('mongoDB.connect : ok')
})
mongoose.Promise = global.Promise

app.use(
  helmet(),
  compression(),
  cors(cfg.cors_opt),
  logger('dev'),
  bodyParser.urlencoded({ extended: false }),
  bodyParser.json({ type: 'application/json' }),
  cookieParser(),
  express.static(path.join(__dirname, 'public')),
  routes // app routes
)

app.listen(cfg.port, () => {
  console.log(`product version : ${cfg.versionEn}`)
  console.log(`express.listen.port : ${cfg.port}`)
})

module.exports = app
