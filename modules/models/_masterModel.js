// eslint-disable-next-line import/no-unresolved
const User = require('./member/User')
const Admin = require('./member/Admin')
const Manager = require('./member/Manager')

module.exports = { User, Admin, Manager }
