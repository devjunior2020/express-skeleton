const Mongoose = require('mongoose')
const UniqueValidator = require('mongoose-unique-validator')

let { Schema } = Mongoose
Schema = new Schema(
  {
    adminFlag: { type: Boolean, default: true },
    // فعال === 1  &&  غیرفعال === 0 && حذف === 2
    status: { type: Number, default: 1 },
    trackingCode: { type: Number, unique: true },

    name: { type: String, default: '' },
    family: { type: String, default: '' },
    // 1 === male && 2 === famel
    gender: { type: Number, default: 1 },
    email: { type: String, default: '' },
    address: { type: String, default: '' },
    national_code: { type: String, default: '' },
    birthday: { type: Date, default: '' },
    foreign_national: { type: String, default: '' },
    postal_code: { type: String, default: '' },
    personal: { type: Boolean, default: true },
    telephone: { type: String, default: '' },
    laws: { type: Boolean, default: 1 },
    mobile: { type: String, required: true, unique: true },
    geom: {
      lon: { type: String, default: '' },
      len: { type: String, default: '' }
    },
    location: { type: String, default: '' },
    province: { type: String, default: '' },
    region: { type: String, default: '' },
    city: { type: String, default: '' },
    profile_img: { type: String, default: '' },
    birth_img: { type: String, default: '' },
    national_img: { type: String, default: '' }
  },
  { timestamps: true }
)
Schema.plugin(UniqueValidator)
module.exports = Mongoose.model('User', Schema)
