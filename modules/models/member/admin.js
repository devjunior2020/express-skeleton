const Mongoose = require('mongoose')
const UniqueValidator = require('mongoose-unique-validator')

let { Schema } = Mongoose
Schema = new Schema(
  {
    adminFlag: { type: Boolean, default: true },
    mobile: { type: String, required: true, unique: true },
    // فعال === 1  &&  غیرفعال === 0 && حذف === 2
    status: { type: Number, default: 1 },
    trackingCode: { type: Number, unique: true },
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    password: { type: String, required: true },
    code: { type: String, default: '0' },
    date_code: { type: Date },
    repeat: { type: Number, default: 0 }
  },
  { timestamps: true }
)

Schema.plugin(UniqueValidator)
module.exports = Mongoose.model('Admin', Schema)
