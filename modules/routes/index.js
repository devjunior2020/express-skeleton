const express = require('express')

const router = express.Router()

router.all('/', (req, res) =>
  res.status(200).json({
    data: null,
    message: cfg.versionEn,
    error: null,
    success: true,
  })
)

module.exports = router
