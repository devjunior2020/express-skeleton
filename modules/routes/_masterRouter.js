const express = require('express')

const index = require('./index')
const panel = require('./panel')
// const blog = require('./blog')
// const shop = require('./shop')
// const library = require('./library')

const router = express.Router()

router.use('/', index)
router.use('/panel', panel)

// بررسی روت ناشناخته
router.use((req, res) =>
  res.status(404).json({
    data: null,
    message: 'page not found ...',
    error: '404',
    success: false,
  })
)

// router.use((err, req, res) => {
//   // if (err.hasOwnProperty('code') && err.code === 'LIMIT_FILE_SIZE') {
//   if (err.code === 'LIMIT_FILE_SIZE') {
//     return res.status(500).json({
//       data: {},
//       message: 'حجم فایل بیش از 1 مگابایت است.',
//       error: '',
//       success: false,
//     })
//   } else {
//     return res.status(500).json({
//       data: {},
//       message: err.toString(),
//       error: '',
//       success: false,
//     })
//   }
// })

module.exports = router
